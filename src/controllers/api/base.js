import axios from 'axios';

export class APIRequest {
  constructor(method = 'get', url) {
    this.method = method;
    this.url = url;
  }

  exec() {
    let url = this.url;

    return axios[this.method](url)
      .then((response) => {
        return response.data;
      }).catch((error) => {
        throw new Error(error);
      });
  }
}

export function apiGET(url) {
  let req = new APIRequest('get', url);

  return () => req.exec();
}

