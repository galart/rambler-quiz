import React, { Component } from 'react';
import { connect } from 'react-redux';
import Quiz from '../components/Quiz';
import { fetchQuiz, setAnswer } from '../actions/quiz';
import PropTypes from 'prop-types';

class QuizContainer extends Component {
  static propTypes = {
    activeIndex: PropTypes.number,
    fetchQuiz: PropTypes.func,
    questions: PropTypes.array,
    setAnswer: PropTypes.func
  };

  componentDidMount() {
    this.props.fetchQuiz();
  }

  render() {
    const { questions, activeIndex } = this.props;

    return (<Quiz
      questions={questions}
      activeIndex={activeIndex}
      setAnswer={this.props.setAnswer}
      fetchQuiz={this.props.fetchQuiz}
    />);
  }
}

function mapStateToProps(state) {
  return {
    questions: state.quiz.questions,
    activeIndex: state.quiz.activeIndex
  };
}

function mapDispatchToProps(dispatch) {
  return {
    fetchQuiz: () => {
      dispatch(fetchQuiz());
    },
    setAnswer: (answer) => {
      dispatch(setAnswer(answer));
    }
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(QuizContainer);

