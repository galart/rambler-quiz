import { createStore, applyMiddleware } from 'redux';
import rootReducer from './reducers/root';
import thunk from 'redux-thunk';

let middlewares = [];

const { createLogger } = require('redux-logger');

const logger = createLogger({
  collapsed: true,
  level: () => 'info',
  colors: {
    prevState: () => 'indianred',
    action: () => 'steelblue',
    nextState: () => 'limegreen'
  }
});

middlewares.push(thunk);
middlewares.push(logger);

const store = createStore(rootReducer, applyMiddleware(...middlewares));

export default store;
