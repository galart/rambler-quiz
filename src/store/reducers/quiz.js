import { FETCH_QUIZ, SET_ANSWER } from '../../constants/quiz';

const initialState = {
  questions: [],
  activeIndex: null
};

export default function quizzes(state = initialState, action) {
  switch (action.type) {
    case FETCH_QUIZ: {
      return { ...state, questions: action.questions, activeIndex: 0 };
    }
    case SET_ANSWER: {
      return { ...state, questions: state.questions.map((question, i)=>{
        if (i === action.activeIndex) {
          return {
            ...question,
            isCorrect: action.isCorrect,
            selectedAnswers: action.selectedAnswers
          };
        }
        return question;
      }), activeIndex: ++action.activeIndex };
    }
    default:
      return state;
  }
}
