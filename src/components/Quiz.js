import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Question from './Question';
import Results from './Results';

class Quiz extends Component {
  static propTypes = {
    activeIndex: PropTypes.number,
    fetchQuiz: PropTypes.func,
    questions: PropTypes.array,
    setAnswer: PropTypes.func
  };

  render() {
    const { questions, activeIndex, setAnswer, fetchQuiz } = this.props;

    if (activeIndex !== null) {
      let activeQuestion = questions[activeIndex];
      if (activeQuestion) {
        return (<div className={'quiz'}>
          <Question
            data={activeQuestion}
            activeIndex={activeIndex}
            setAnswer={setAnswer}
          />
        </div>);
      }
      return (<Results
        questions={questions}
        fetchQuiz={fetchQuiz}
      />);
    }
    return <div className={'quiz'}><p>Загрузка...</p></div>;
  }
}

export default Quiz;

