import React, { Component } from 'react';
import PropTypes from 'prop-types';

class Question extends Component {
  static propTypes = {
    activeIndex: PropTypes.number,
    data: PropTypes.object,
    setAnswer: PropTypes.func
  };

  constructor(props) {
    super(props);
    this.state = {
      answers: [...this.props.data.incorrect_answers, this.props.data.correct_answer].sort(this.shuffleAnswers),
      selectedAnswers: []
    };
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.activeIndex !== this.props.activeIndex) {
      this.setState({
        answers: [...nextProps.data.incorrect_answers, nextProps.data.correct_answer].sort(this.shuffleAnswers),
        selectedAnswers: []
      });
    }
  }

  shuffleAnswers() {
    return Math.random() - 0.5;
  }

  checkAnswerHandler(e) {
    const { selectedAnswers } = this.state;

    if (this.props.data.type === 'multiple') {
      if (selectedAnswers.indexOf(e.target.id) !== -1) {
        this.setState({ selectedAnswers: this.state.selectedAnswers.filter((answer)=>{
          return answer !== e.target.id;
        }) });
      } else {
        this.setState({ selectedAnswers: this.state.selectedAnswers.concat([e.target.id]) });
      }
    } else {
      this.setState({ selectedAnswers: [e.target.id] });
    }
  }

  sendAnswer = () => {
    const { selectedAnswers } = this.state;
    let data = {};
    if (
      (
        this.props.data.type === 'multiple' && // TODO selectedAnswers.length > 1 потому что в api даже вопросы со множественным выбором всегда имеют только ОДИН правильный ответ
        (selectedAnswers.length > 1 || selectedAnswers[0] !== this.props.data.correct_answer)
      ) ||
      (
        this.props.data.type === 'boolean' &&
        selectedAnswers[0] !== this.props.data.correct_answer
      )
    ) {
      data.isCorrect = false;
    } else if (selectedAnswers[0] === this.props.data.correct_answer) {
      data.isCorrect = true;
    }
    data.selectedAnswers = selectedAnswers;
    data.activeIndex = this.props.activeIndex;
    this.props.setAnswer(data);
  };

  render() {
    const { data } = this.props;
    const { answers, selectedAnswers } = this.state;

    return (<div className={'question form-group'}>
      <h3 dangerouslySetInnerHTML={{ __html: data.question }} />
      {
        <div className='answers-list'>
          {answers.map((answer, i)=>{
            return (<div className={'form-check'} key={data.question + i}>
              <input
                id={answer}
                name={'answer'}
                className={'form-check-input'}
                type={data.type === 'multiple' ? 'checkbox' : 'radio'}
                onChange={this.checkAnswerHandler.bind(this)}
              />
              <label
                htmlFor={answer}
                className={'form-check-label'}
                dangerouslySetInnerHTML={{ __html: answer }}
              />
            </div>);
          })}
        </div>
      }
      <button
        type={'button'}
        disabled={selectedAnswers.length === 0}
        className={'btn btn-primary'}
        onClick={this.sendAnswer.bind(this)}
      >Ответить</button>
    </div>);
  }
}

export default Question;

