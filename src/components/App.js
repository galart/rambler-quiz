import React, { Component } from 'react';
import logo from '../logo.svg';
import QuizContainer from '../containers/QuizContainer';

class App extends Component {
  render() {
    return (
      <div className='app'>
        <div className='app-header-wrapper'>
          <header className='app-header'>
            <img alt='logo' src={logo}
              className='app-logo'/>
            <h1 className='app-title'>QUIZ</h1>
          </header>
        </div>
        <QuizContainer />
      </div>
    );
  }
}

export default App;
