import React, { Component } from 'react';
import PropTypes from 'prop-types';
import correctIcon from '../check_circle.svg';
import notCorrectIcon from '../error_circle.svg';

export default class Results extends Component {
  static propTypes = {
    fetchQuiz: PropTypes.func,
    questions: PropTypes.array
  };

  summary = this.props.questions.reduce((acc, curr)=>{
    if (!acc[curr.difficulty]) {
      acc[curr.difficulty] = { correct: 0, not_correct: 0, total: 0 };
    }

    if (curr.isCorrect) {
      acc[curr.difficulty].correct = ++acc[curr.difficulty].correct;
    } else {
      acc[curr.difficulty].not_correct = ++acc[curr.difficulty].not_correct;
    }
    acc[curr.difficulty].total = ++acc[curr.difficulty].total;

    return acc;
  }, {});

  buildSummaryTable = function() {
    return (<table className='summary'>
      <tbody>
        {Object.keys(this.summary).map(function(type, i) {
          return (<tr key={i}>
            <td>
              {`${type}: всего - ${this[type].total}, правильно - ${this[type].correct}, неправильно - ${this[type].not_correct}`}
            </td>
          </tr>);
        }, this.summary)}
      </tbody>
    </table>);
  };

  restart = () => {
    this.props.fetchQuiz();
  };

  render() {
    return (<div className='results'>
      <button onClick={this.restart} type='button'
        className='btn btn-link'>&lt; Начать сначала</button>
      <h3>Результаты</h3>
      {this.buildSummaryTable()}
      {
        this.props.questions.map((data, i) => {
          return (<div className='question-result' key={i}>
            <h4>
              <img alt={data.isCorrect ? 'correct_icon' : 'fail_icon'} src={data.isCorrect ? correctIcon : notCorrectIcon}/>
              <span dangerouslySetInnerHTML={{ __html: data.question }} />
            </h4>
            <div className='selected-answers'>
              <h5>Выбранные ответы</h5>
              {data.selectedAnswers.map((answers, j)=>{
                return <p key={j} dangerouslySetInnerHTML={{ __html: answers }} />;
              })}
            </div>
            <div className='correct-answer'>
              <h5>Правильный ответ</h5>
              <p>{data.correct_answer}</p>
            </div>
          </div>);
        })
      }
    </div>);
  }
}
