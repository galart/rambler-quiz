import { apiGET } from '../controllers/api/base';
import { FETCH_QUIZ, SET_ANSWER } from '../constants/quiz';

const apiMethods = {
  getQuiz: apiGET('https://opentdb.com/api.php?amount=10'),
};

// TODO если API opentdb.com вдруг не отвечает(очень часто в дауне) то раскомментировать код ниже
// export function fetchQuiz() {
//   return function(dispatch){
//       dispatch({
//         type: FETCH_QUIZ,
//         questions: [{"category":"General Knowledge","type":"boolean","difficulty":"medium","question":"The term &quot;Spam&quot; came before the food product &quot;Spam&quot;.","correct_answer":"False","incorrect_answers":["True"]},{"category":"Geography","type":"multiple","difficulty":"hard","question":"Where is the fast food chain &quot;Panda Express&quot; headquartered?","correct_answer":"Rosemead, California","incorrect_answers":["Sacremento, California","Fresno, California","San Diego, California"]},{"category":"Geography","type":"multiple","difficulty":"hard","question":"What is the area of Vatican City?","correct_answer":"0.44km^2","incorrect_answers":["0.10km^2","0.86km^2","12.00km^2"]},{"category":"Entertainment: Musicals & Theatres","type":"multiple","difficulty":"easy","question":"How many plays is Shakespeare generally considered to have written?","correct_answer":"37","incorrect_answers":["18","54","25"]},{"category":"Entertainment: Video Games","type":"multiple","difficulty":"easy","question":"In what year was &quot;Antichamber&quot; released?","correct_answer":"2013","incorrect_answers":["2012","2014","2011"]},{"category":"Entertainment: Music","type":"boolean","difficulty":"hard","question":"The band STRFKR was also briefly known as Pyramiddd.","correct_answer":"True","incorrect_answers":["False"]},{"category":"General Knowledge","type":"boolean","difficulty":"medium","question":"Fast food restaurant chains Carl&#039;s Jr. and Hardee&#039;s are owned by the same company.","correct_answer":"True","incorrect_answers":["False"]},{"category":"Science & Nature","type":"multiple","difficulty":"easy","question":"What is the unit of electrical resistance?","correct_answer":"Ohm","incorrect_answers":["Mho","Tesla","Joule"]},{"category":"Entertainment: Film","type":"boolean","difficulty":"easy","question":"Actor Tommy Chong served prison time.","correct_answer":"True","incorrect_answers":["False"]},{"category":"Entertainment: Television","type":"boolean","difficulty":"medium","question":"In &quot;Star Trek: The Next Generation&quot;, Data is the only android in existence.","correct_answer":"False","incorrect_answers":["True"]}]
//       })
//   }
// }

// TODO а этот закомментировать
export function fetchQuiz() {
  return function(dispatch) {
    apiMethods.getQuiz().then((data)=>{
      dispatch({
        type: FETCH_QUIZ,
        questions: data.results
      });
    });
  };
}

export function setAnswer(answer) {
  return function(dispatch) {
    dispatch({
      type: SET_ANSWER,
      selectedAnswers: answer.selectedAnswers,
      isCorrect: answer.isCorrect,
      activeIndex: answer.activeIndex
    });
  };
}
